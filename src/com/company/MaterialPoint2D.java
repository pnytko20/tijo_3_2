package com.company;

public class MaterialPoint2D extends Point2D {
    private double mass;

    public MaterialPoint2D(double x, double y, double mass) {
        this.x = x;
        this.y = y;
        this.mass = mass;
    }

    public double getMass() {
        return mass;
    }

    @Override
    public String toString() {
        return this.x + " | " + this.y + " | " + this.mass;
    }
}
