package com.company;

public class Calculations {

    public static Point2D positionGeometricCenter(Point2D[] point) {
        double centroidX = 0, centroidY = 0;

        for (Point2D coordinates : point) {
            centroidX += coordinates.getX();
            centroidY += coordinates.getY();
        }
        return new Point2D(centroidX / point.length, centroidY / point.length);
    }

    public static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoint) {
        double x = 0, y = 0, mass = 0;
        for (MaterialPoint2D coordinates : materialPoint) {
            x += coordinates.getX() * coordinates.getMass();
            y += coordinates.getY() * coordinates.getMass();
            mass += coordinates.getMass();
        }
        return new MaterialPoint2D(x / mass, y / mass, mass);
    }
}
